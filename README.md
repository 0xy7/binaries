# Binaries
These binaries are provided as-is. Ensure you have the necessary permissions to use and distribute them. The repository owner is not responsible for any misuse or issues resulting from the use of these binaries.

## Available Binaries
- [Villan](https://github.com/t3l3machus/Villain)

## Usage
```bash
git clone https://gitlab.com/0xy7/binaries.git
cd binaries
```
### Move Binaries to /usr/bin (Optional):
If you want to make the binaries accessible system-wide, you can move them to the /usr/bin directory.
```bash
cd binaries
sudo mv * /usr/bin
```

## Contributing
Feel free to contribute by adding new binaries or improving the organization of the repository.

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/0xy7/binaries/-/raw/main/LICENSE)